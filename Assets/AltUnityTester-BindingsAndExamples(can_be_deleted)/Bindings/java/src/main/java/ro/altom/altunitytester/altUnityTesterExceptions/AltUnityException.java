package ro.altom.altunitytester.altUnityTesterExceptions;

public class AltUnityException extends Exception {
    public AltUnityException() {

    }

    public AltUnityException(String message) {
        super(message);

    }
}

